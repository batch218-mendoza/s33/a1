
// task 1 and 2

fetch('https://jsonplaceholder.typicode.com/todos') 
.then((response) => response.json())
.then((json) =>  

{
   let titleOnly = json.map(function(data){
    return data.title;
})
console.log(titleOnly);
});


// task 3 and 4

fetch('https://jsonplaceholder.typicode.com/todos/21')
.then((response)=>response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`))



// task 5

fetch('https://jsonplaceholder.typicode.com/todos', {

    method: 'POST',

    headers: {
        'Content-type' : 'application/json'
    },
    body: JSON.stringify({
        title: 'Created to do list Item',
        completed: false,
        userId: 1
    })

})              
.then((response)=>response.json())
.then((json)=> console.log(json));


//task 6
fetch('https://jsonplaceholder.typicode.com/todos/1', 
{
    method: 'PUT',
    headers: {
        'Content-type' : 'application/json'
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        description: "To update the my to do list with a different data structure.",
        status: "Pending",
        title: "Updated to do list item",
        userId: 1,
    })  
})
.then((response)=>response.json())
.then((json)=> console.log(json));


//task 7

fetch('https://jsonplaceholder.typicode.com/todos/1',{
    method: "PATCH",
    headers: {
        'Content-type' : 'application/json'
    }, 
    body: JSON.stringify({
       status: "Complete",
       dateCompleted: "22/11/2022"
    })  
})
.then((response)=>response.json())
.then((json)=> console.log(json));



//task 8


fetch('https://jsonplaceholder.typicode.com/todos/70',{
    method: "DELETE",
})
.then((response)=>response.json())
.then((json)=> console.log(json));